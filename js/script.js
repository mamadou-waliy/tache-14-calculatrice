class Calculator {
    constructor(previousElementText, currentElementText) {
        this.previousElementText = previousElementText;
        this.currentElementText = currentElementText;
        this.clear()
    }
    clear() {
        this.currentOperand = ''
        this.previousOperand = ''
        this.operator = undefined
    }
    delete() {
        this.currentOperand = this.currentOperand.toString().slice(0, -1)
    }
    chooseOperator(operator) {
        if (this.currentOperand === '') return
        if (this.previousOperand !== '') {
            this.cumpute()
        }
        this.operator = operator
        this.previousOperand = this.currentOperand
        this.currentOperand = ''
    }
    appendNumber(number) {
        if (number === '.' && this.currentOperand.includes('.')) return
        this.currentOperand = this.currentOperand.toString() + number.toString()
    }
    formatDisplayNumber(number) {
        const stringNumber = number.toString()
        const integerNumber = parseFloat(stringNumber.split('.')[0])
        const decimalNumber = stringNumber.split('.')[1]
        let integerDisplay
        if (isNaN(integerNumber)) {
            integerDisplay = ''
        } else {
            integerDisplay = integerNumber.toLocaleString('fr', {
                maximumFractionDigits: 0
            })
        }
        if (decimalNumber != null) {
            return `${integerDisplay}.${decimalNumber}`
        } else {
            return integerDisplay
        }

    }
    update() {
        this.currentElementText.innerText = this.formatDisplayNumber(this.currentOperand)
        if (this.operator != null) {
            this.previousElementText.innerText =
                `${this.formatDisplayNumber(this.previousOperand)} ${this.operator}`
        } else {
            this.previousElementText.innerText = ''
        }

    }
    cumpute() {
        let cumputation
        const prev = parseFloat(this.previousOperand)
        const current = parseFloat(this.currentOperand)
        if (isNaN(prev) || isNaN(current)) return
        switch (this.operator) {
            case '+':
                cumputation = prev + current
                break
            case '-':
                cumputation = prev - current
                break
            case '÷':
                cumputation = prev / current
                break
            case '*':
                cumputation = prev * current
                break
            case 'mod':
                cumputation = prev % current
                break
            case 'Sin(x)':
                cumputation = prev * (Math.sin(prev))
                break
            case 'Cos(x)':
                cumputation = prev * (Math.cos(current))
                break
            case 'Tan(x)':
                cumputation = prev * (Math.tan(current))
                break
            case 'exp(x)':
                cumputation = prev * (Math.exp(current))
                break
            case 'Log':
                cumputation = prev * (Math.log10(current))
                break
            case 'x<sup>n</sup>':
                cumputation = Math.pow(prev, current)
                break
            case '√x':
                cumputation = prev * (Math.sqrt(current))
                break


            default:
                return
        }
        this.currentOperand = cumputation
        this.operator = undefined
        this.previousOperand = ''
    }

}

const numberButtons = document.querySelectorAll('[data-number]')
const operatorButtons = document.querySelectorAll('[data-operator]')
const equalsButton = document.querySelector('[data-equals]')
const clearAllButton = document.querySelector('[data-clear-all]')
const deleteButton = document.querySelector('[data-delete]')
const previousElementText = document.querySelector('[data-prev-operand]')
const currentElementText = document.querySelector('[data-current-operand]')

const calculator = new Calculator(previousElementText, currentElementText)

numberButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.appendNumber(button.innerHTML)
        calculator.update()
    })
})
operatorButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.chooseOperator(button.innerHTML)
        calculator.update()
    })
})
equalsButton.addEventListener('click', button => {
    calculator.cumpute()
    calculator.update()
})
clearAllButton.addEventListener('click', button => {
    calculator.clear()
    calculator.update()
})
deleteButton.addEventListener('click', button => {
    calculator.delete()
    calculator.update()
})